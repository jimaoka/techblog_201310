public with sharing class ConfirmTestController {
   public List<AccountDto> accDtos {get; set;}
   
   public void initAction(){
      accDtos = new List<AccountDto>();
      for(Account acc : [Select Id, Name From Account Order By Name Limit 5]){
         AccountDto dto = new AccountDto(this, acc);
         accDtos.add(dto);
      }
   }
   
   public class AccountDto{
      private ConfirmTestController controller;
      public Account acc {get; set;}
      
      public AccountDto(ConfirmTestController controller, Account acc){
         this.controller = controller;
         this.acc = acc;
      }
      
      public Pagereference doDelete(){
         delete acc;
         controller.initAction();
         return null;
      }
   }
}
